package com.example.demo;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DemoServiceTest {

    private DemoService service;

    @Test
    public void addtest() {
        service = new DemoService();
        Assert.assertEquals(7, service.addition(3, 4));
    }

}