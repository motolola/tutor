package com.example.demo;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

    public int addition(int i, int j) {

        return i + j;
    }
}
